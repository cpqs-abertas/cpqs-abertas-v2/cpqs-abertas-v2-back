from models.models import Pessoa, ProducaoArtistica
import util.fo.route_operations as route_operations
import util.populate.code.parsing.db.connection as db_connection


mongo_client = db_connection.get_mongo_client()

def get_count(params):
    return route_operations.count(ProducaoArtistica, params)


def get_keywords(params):
    return route_operations.keywords(ProducaoArtistica, params)


def get_map(params):
    return route_operations.map(ProducaoArtistica, params)


def get_national_map(params):
    return route_operations.national_map(ProducaoArtistica, params)


def get_types_count(params):
    return route_operations.types_count(ProducaoArtistica, params)


def get_types():
    types = ProducaoArtistica.objects \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    result = []
    accepted = ['Outra_Producao_Artistica_Cultural', 'Artes_Visuais', 'Musica']
    for t in types:
        if t['tipo'] in accepted:
            result.append(t['tipo'])
    return result


def get_aggregated_count(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_artistica")
    return aggregates.find_one({ "operation": "count" })["result"]


def get_aggregated_count_by_type(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_artistica")
    return aggregates.find_one({ "operation": "count_by_type" })["result"]


def get_aggregated_keywords(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_artistica")
    return aggregates.find_one({ "operation": "keywords" })["result"]


def get_aggregated_map(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_artistica")
    return aggregates.find_one({ "operation": "map" })["result"]


def get_aggregated_national_map(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_artistica")
    return aggregates.find_one({ "operation": "national_map" })["result"]


def get_aggregated_types_count(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_artistica")
    return aggregates.find_one({ "operation": "types_count" })["result"]
