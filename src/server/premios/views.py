from django.http import JsonResponse
from . import services
from util.fo.RequestError import RequestError
from util.fo.route_operations import error

def count(request):
    """Count for premios route.

    Parameters: a GET request with 'ano_inicio', 'ano_fim' and
    'departamentos[]' parameters.

    Returns:
    JsonResponse with status code and the count of premios for each year
    for each departamento.
   """
    try:
        return JsonResponse(services.get_count(request.GET))
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)

def aggregated_count(request):
    return JsonResponse(services.get_aggregated_count(request.GET))
