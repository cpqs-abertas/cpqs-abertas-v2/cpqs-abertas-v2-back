from django.db.models import Count
from models.models import Pessoa, PremiosTitulos
import util.fo.route_operations as route_operations
import util.populate.code.parsing.db.connection as db_connection
from util.format import make_chart_js
from util.fo.RequestError import RequestError

mongo_client = db_connection.get_mongo_client()

def get_count(params):
    ini_year = params.get('ano_inicio', '1948')
    end_year = params.get('ano_fim', '2019')
    departments = params.getlist('departamentos[]', '')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        raise RequestError('Ano inválido', 400)

    ini_year, end_year = int(ini_year), int(end_year)

    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    has_request_departments = bool(departments)

    if not departments:
        departments = [dep['departamento']
                       for dep in Pessoa.objects.using(f'{params.get("institute")}_db').values('departamento')
                       .distinct()]

    result = {
        dep:
            {
                "tipo_unico": {year: 0 for year in labels}
            }
        for dep in departments
    }

    prods = PremiosTitulos.objects.using(f'{params.get("institute")}_db')
    if has_request_departments:
        authors = Pessoa.objects.using(f'{params.get("institute")}_db').values('id_lattes')
        authors_from_deps = authors.filter(departamento__in=departments)
        prods = prods.filter(pessoa__in=authors_from_deps)

    prods = prods \
        .values('pessoa__departamento', 'ano') \
        .annotate(year_total=Count('id', distinct=True)) \
        .order_by('pessoa__departamento', 'ano')

    prods = list(prods)
    department = ""
    i = 0
    while i < len(prods):
        prod = prods[i]
        if prod['pessoa__departamento'] != department:
            department = prod['pessoa__departamento']
            while (i < len(prods)
                    and prods[i]['pessoa__departamento'] == department):
                prod = prods[i]
                if ini_year <= prod['ano'] <= end_year:
                    result[department]['tipo_unico'][prod['ano']] = \
                        prod['year_total']
                i += 1

    if format == 'chartjs':
        result = make_chart_js(result)

    return result

def get_aggregated_count(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "premios_titulos")
    return aggregates.find_one({ "operation": "count" })["result"]