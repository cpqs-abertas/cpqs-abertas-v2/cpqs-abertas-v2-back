from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /orientacao/count
    url(r'^count$', views.count, name='orientacao-count'),
    # ex: /orientacao/aggregated-count
    url(r'^aggregated-count$', views.aggregated_count, name='orientacao-aggregated_count'),
    # ex: /orientacao/aggregated-count-by-type
    url(r'^aggregated-count-by-type$', views.aggregated_count_by_type, name='orientacao-aggregated_count_by_type'),

    # ex: /orientacao/keywords
    url(r'^keywords$', views.keywords, name='orientacao-keywords'),
    # ex: /orientacao/aggregated-keywords
    url(r'^aggregated-keywords$', views.aggregated_keywords, name='orientacao-aggregated_keywords'),

    # ex: /orientacao/map
    url(r'^map$', views.map, name='orientacao-map'),
    # ex: /orientacao/aggregated-map
    url(r'^aggregated-map$', views.aggregated_map, name='orientacao-aggregated_map'),

    # ex: /orientacao/types-count
    url(r'^types-count$', views.types_count, name='orientacao-types_count'),
]
