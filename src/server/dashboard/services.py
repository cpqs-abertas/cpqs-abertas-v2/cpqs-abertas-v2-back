import datetime
import json
import os

from django.db.models import Count

from models.models import Pessoa, ProducaoArtistica, ProducaoTecnica, ProducaoBibliografica, Orientacao, Bancas, PremiosTitulos
import util.fo.route_operations as route_operations
import util.populate.code.parsing.db.connection as db_connection
from util.populate.code.parsing.util import normalize_string


# INSTITUTE = os.environ.get('INSTITUTE', '')

mongo_client = db_connection.get_mongo_client()

def get_dashboard(institute):
    dict = {'Produção Artística': [ProducaoArtistica, 'id'],
            'Produção Técnica': [ProducaoTecnica, 'id'],
            'Produção Bibliográfica': [ProducaoBibliografica, 'id'],
            'Orientação': [Orientacao, 'id'],
            'Bancas': [Bancas, 'id'],
            'Prêmios e Títulos': [PremiosTitulos, 'id']}

    result = {}
    for key, value in dict.items():
        result[key] = value[0].objects.using(f'{institute}_db')\
            .aggregate(Count(value[1]))[value[1] + '__count']

    return result


def get_count(params):
    now = datetime.datetime.now()
    current_year = str(now.year)
    institute = params.get("institute")
    ini_year = params.get('ano_inicio', '1948')
    end_year = params.get('ano_fim', current_year)
    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')
    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year) + 1)]
    result = {institute.upper(): {year: 0 for year in labels}}

    prodsArt = ProducaoArtistica.objects.using(f'{params.get("institute")}_db').all().distinct()
    prodsArt = prodsArt.values('id', 'ano')
    prodsTec = ProducaoTecnica.objects.using(f'{params.get("institute")}_db').all().distinct()
    prodsTec = prodsTec.values('id', 'ano')
    prodsBib = ProducaoBibliografica.objects.using(f'{params.get("institute")}_db').all().distinct()
    prodsBib = prodsBib.values('id', 'ano')
    prodsBancas = Bancas.objects.using(f'{params.get("institute")}_db').all().distinct()
    prodsBancas = prodsBancas.values('id', 'ano')
    prodsOri = Orientacao.objects.using(f'{params.get("institute")}_db').all().distinct()
    prodsOri = prodsOri.values('id', 'ano')
    prodsPremios = PremiosTitulos.objects.using(f'{params.get("institute")}_db').all().distinct()
    prodsPremios = prodsPremios.values('id', 'ano')

    prods = list(prodsArt.union(prodsTec, prodsBib, prodsBancas, prodsOri,
                                prodsPremios, all=True))

    for prod in prods:
        if (prod['ano'] >= ini_year and prod['ano'] <= end_year):
            result[institute.upper()][prod['ano']] += 1

    return result

def get_map(params):
    try:
        limit = int(params.get('limit', 20))
    except ValueError:
        return route_operations.error('Limite inválido.')
    if limit <= 0:
        return route_operations.error('Limite inválido.')
    department = params.get('departamento', '')

    countriesProdArt = ProducaoArtistica.objects.using(f'{params.get("institute")}_db').distinct()
    countriesProdBib = ProducaoBibliografica.objects.using(f'{params.get("institute")}_db').distinct()
    countriesProdTec = ProducaoTecnica.objects.using(f'{params.get("institute")}_db').distinct()
    countriesBancas = Bancas.objects.using(f'{params.get("institute")}_db').distinct()
    countriesOri = Orientacao.objects.using(f'{params.get("institute")}_db').distinct()

    if department:
        author_from_dep = Pessoa.objects.using(f'{params.get("institute")}_db') \
            .filter(departamento__contains=department) \
            .values('id_lattes')

        countriesProdArt = countriesProdArt.filter(autores__in=author_from_dep)
        countriesProdBib = countriesProdBib.filter(autores__in=author_from_dep)
        countriesProdTec = countriesProdTec.filter(autores__in=author_from_dep)
        countriesBancas = countriesBancas \
            .filter(participantes__in=author_from_dep)
        countriesOri = countriesOri.filter(pessoa__in=author_from_dep)

    countriesProdArt = countriesProdArt \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]
    countriesProdBib = countriesProdBib \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]
    countriesProdTec = countriesProdTec \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]
    countriesBancas = countriesBancas \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]
    countriesOri = countriesOri \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]

    result = []

    with open('util/countries.json', 'r') as f:
        country_dict = json.loads(f.read())

    countryList = [countriesProdArt, countriesProdBib, countriesProdTec,
                   countriesBancas, countriesOri]

    countries = {}
    for i, obj in enumerate(countryList):
        for country in obj:
            key = normalize_string(country['pais'])
            try:
                geolocation = country_dict[key]
                if geolocation["name"] not in countries:
                    countries[geolocation["name"]] = {
                        'lat': geolocation['lat'],
                        'long': geolocation['long'],
                        'total': country['total']
                    }
                else:
                    countries[geolocation["name"]]['total'] += country['total']
            except Exception:
                pass

    for geo in countries:
        result.append({geo: countries[geo]})
    
    return result

def get_national_map(params):
    try:
        limit = int(params.get('limit', 20))
    except ValueError:
        return route_operations.error('Limite inválido')

    if not limit > 0:
        return route_operations.error('Limite inválido')

    department = params.get('departamento', '')

    prod_art_states_count = ProducaoArtistica.objects.using(f'{params.get("institute")}_db').all()
    prod_bib_states_count = ProducaoBibliografica.objects.using(f'{params.get("institute")}_db').all()
    prod_tec_states_count = ProducaoTecnica.objects.using(f'{params.get("institute")}_db').all()

    if department:
        author_from_dep = Pessoa.objects.using(f'{params.get("institute")}_db') \
            .filter(departamento__contains=department) \
            .values('id_lattes')

        prod_art_states_count = prod_art_states_count \
            .filter(autores__in=author_from_dep)
        prod_bib_states_count = prod_bib_states_count \
            .filter(autores__in=author_from_dep)
        prod_tec_states_count = prod_tec_states_count \
            .filter(autores__in=author_from_dep)

    prod_art_states_count = prod_art_states_count \
        .values('estado') \
        .annotate(total=Count('estado'))
    prod_bib_states_count = prod_bib_states_count \
        .values('estado') \
        .annotate(total=Count('estado'))
    prod_tec_states_count = prod_tec_states_count \
        .values('estado') \
        .annotate(total=Count('estado'))

    states = {
        None: 0,
        "": 0,
        "Acre": 0,
        "Alagoas": 0,
        "Amapá": 0,
        "Amazonas": 0,
        "Bahia": 0,
        "Ceará": 0,
        "Distrito Federal": 0,
        "Espírito Santo": 0,
        "Goiás": 0,
        "Maranhão": 0,
        "Mato Grosso": 0,
        "Mato Grosso do Sul": 0,
        "Minas Gerais": 0,
        "Pará": 0,
        "Paraíba": 0,
        "Paraná": 0,
        "Pernambuco": 0,
        "Piauí": 0,
        "Rio de Janeiro": 0,
        "Rio Grande do Norte": 0,
        "Rio Grande do Sul": 0,
        "Rondônia": 0,
        "Roraima": 0,
        "Santa Catarina": 0,
        "São Paulo": 0,
        "Sergipe": 0,
        "Tocantins": 0
    }

    for state in prod_art_states_count:
        states[state["estado"]] += state["total"]
    for state in prod_bib_states_count:
        states[state["estado"]] += state["total"]
    for state in prod_tec_states_count:
        states[state["estado"]] += state["total"]

    if "" in states:
        del states[""]
    if None in states:
        del states[None]

    states_total = sum([total for total in states.values()])
    result = {"estados": states, "total": states_total}

    return result

def get_aggregated_dashboard(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "dashboard")
    return aggregates.find_one({ "operation": "dashboard" })["result"]


def get_aggregated_count(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "dashboard")
    return aggregates.find_one({ "operation": "count" })["result"]


def get_aggregated_map(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "dashboard")
    return aggregates.find_one({ "operation": "map" })["result"]


def get_aggregated_national_map(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "dashboard")
    return aggregates.find_one({ "operation": "national_map" })["result"]
