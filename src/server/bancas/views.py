from django.http import JsonResponse
from . import services
from util.fo.RequestError import RequestError
from util.fo.route_operations import error

def count(request):
    """Count for bancas route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'departamentos[]' and 'tipos[]' parameters.

    Returns:
    JsonResponse with status code and the count of bancas
    for each year for each departament.
    """
    try:
        return JsonResponse(services.get_count(request.GET))
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)


def keywords(request):
    """Keywords for bancas route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and a list of words with its frequencies.
    """
    try:
        return JsonResponse({"keywords": services.get_keywords(request.GET)})
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)


def map(request):
    """Map for producao_artistica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of countries
    and the specific artistic productions counter.
    """
    try:
        return JsonResponse({"countries": services.get_map(request.GET)})
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)


def types_count(request):
    """Count_tipos for banas route.

    Parameters: a GET request with 'tipos[]' and 'departamentos[]'.

    Returns:
    JsonResponse with status code and a list with the amount of each
    bancas type.
   """
    try:
        return JsonResponse(services.get_types_count(request.GET))
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)


def aggregated_count(request):
    return JsonResponse(services.get_aggregated_count(request.GET))


def aggregated_count_by_type(request):
    return JsonResponse(services.get_aggregated_count_by_type(request.GET))


def aggregated_keywords(request):
    return JsonResponse({"keywords": services.get_aggregated_keywords(request.GET)})


def aggregated_map(request):
    return JsonResponse({"countries": services.get_aggregated_map(request.GET)})


def aggregated_types_count(request):
    return JsonResponse(services.get_aggregated_types_count(request.GET))