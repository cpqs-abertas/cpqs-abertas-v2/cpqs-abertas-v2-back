from models.models import Pessoa, ProducaoTecnica
import util.fo.route_operations as route_operations
import util.populate.code.parsing.db.connection as db_connection

mongo_client = db_connection.get_mongo_client()

def get_count(params):
    return route_operations.count(ProducaoTecnica, params)


def get_keywords(params):
    return route_operations.keywords(ProducaoTecnica, params)


def get_map(params):
    return route_operations.map(ProducaoTecnica, params)


def get_national_map(params):
    return route_operations.national_map(ProducaoTecnica, params)


def get_types_count(params):
    return route_operations.types_count(ProducaoTecnica, params)


def get_types():
    types = ProducaoTecnica.objects \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    result = []
    accepted = [
        'Trabalho_Tecnico', 'Organizacao_De_Evento',
        'Curso_De_Curta_Duracao_Ministrado', 'Programa_De_Radio_Ou_Tv',
        'Apresentacao_De_Trabalho'
    ]
    for t in types:
        if t['tipo'] in accepted:
            result.append(t['tipo'])
    return result


def get_aggregated_count(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_tecnica")
    return aggregates.find_one({ "operation": "count" })["result"]


def get_aggregated_count_by_type(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_tecnica")
    return aggregates.find_one({ "operation": "count_by_type" })["result"]


def get_aggregated_keywords(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_tecnica")
    return aggregates.find_one({ "operation": "keywords" })["result"]


def get_aggregated_map(params):
    institute = params.dict(params)["institute"]
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_tecnica")
    return aggregates.find_one({ "operation": "map" })["result"]


def get_aggregated_national_map(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_tecnica")
    return aggregates.find_one({ "operation": "national_map" })["result"]


def get_aggregated_types_count(params):
    institute = params.get("institute")
    aggregates = db_connection.select_db_mongo(mongo_client, f'{institute}_aggregate', "producao_tecnica")
    return aggregates.find_one({ "operation": "types_count" })["result"]
