def make_chart_js(response, default_label=''):
    """Utility function that converts a dictionary to the chart.js format.

    Parameters: response: dictionary
                default_label: usually the title of the plot

    Returns:
    Dictionary with labels and datasets.
   """
    result = {}
    datasets = []
    labels = []

    if not response:
        return result

    sample = list(response.values())[0]

    if type(sample) == dict:
        for label, _ in sample.items():
            labels.append(label)
        for k, v in response.items():
            dataset = {}
            dataset['label'] = k
            dataset['data'] = []
            for _, data in v.items():
                dataset['data'].append(data)
            datasets.append(dataset)
    else:
        dataset = {}
        dataset['data'] = []
        dataset['label'] = default_label
        for label, data in response.items():
            labels.append(label)
            dataset['data'].append(data)
        datasets.append(dataset)

    result['datasets'] = datasets
    result['labels'] = labels
    return result
