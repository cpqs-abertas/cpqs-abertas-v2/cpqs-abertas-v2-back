import pymongo
import os
from django.http import QueryDict
from json import dumps, loads

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cpqsabertas.settings')

import sys
sys.path.append("/server")

import django
django.setup()

import dashboard.services as dashboard
import pessoa.services as pessoa
import bancas.services as bancas
import orientacao.services as orientacao
import premios.services as premios_titulos
import producao_artistica.services as producao_artistica
import producao_bibliografica.services as producao_bibliografica
import producao_tecnica.services as producao_tecnica

from parsing.db.connection import aggregate_db_dict

# my_client = pymongo.MongoClient(aggregate_db_dict['host'], 27017)
# my_client.drop_database(aggregate_db_dict['database'])
# db = my_client[aggregate_db_dict['database']]

# count_route_params = QueryDict('ano_inicio=1978')
# keywords_route_params = QueryDict('limit=50')
no_params = QueryDict('')

def create_dict_count_route_params(institute):
    count_route_params = {'institute': institute,  'ano_inicio': '1978'}
    count_route_params_dict = QueryDict('', mutable=True)
    count_route_params_dict.update(count_route_params) 
    return count_route_params_dict

def create_dict_keywords_route_params(institute):
    keywords_route_params = {'institute': institute, 'limit': '50'}
    keywords_route_params_dict = QueryDict('', mutable=True)
    keywords_route_params_dict.update(keywords_route_params)
    return keywords_route_params_dict

def create_dict_default_params(institute):
    default_params = {'institute': institute}
    default_params_dict = QueryDict('', mutable=True)
    default_params_dict.update(default_params)
    return default_params_dict

def add_bancas_aggregates(db, institute):
    collection = db["bancas"]
    aggregates = []
    
    aggregates.append(loads(dumps({"operation": "count", "result": bancas.get_count(create_dict_count_route_params(institute))})))
    aggregates.append(
        loads(dumps({"operation": "keywords", "result": bancas.get_keywords(create_dict_keywords_route_params(institute))})))
    aggregates.append(loads(dumps({"operation": "map", "result": bancas.get_map(create_dict_default_params(institute))})))
    
    types_param = QueryDict(mutable=True)
    default_params = {'institute': institute}
    types_param.update(default_params)
    types_param.setlist('tipos[]', orientacao.get_types())


    aggregates.append(loads(dumps({"operation": "count_by_type", "result": bancas.get_count(types_param)})))

    collection.insert_many(aggregates)


def add_dashboard_aggregates(db, institute):
    collection = db["dashboard"]
    aggregates = []

    count_route_params = {'institute': institute,  'ano_inicio': 1978}
    count_route_params_dict = QueryDict('', mutable=True)
    count_route_params_dict.update(count_route_params) 

    default_params = {'institute': institute}
    default_params_dict = QueryDict('', mutable=True)
    default_params_dict.update(default_params)

    aggregates.append(loads(dumps({"operation": "dashboard", "result": dashboard.get_dashboard(institute)})))
    aggregates.append(loads(dumps({"operation": "count", "result": dashboard.get_count(create_dict_count_route_params(institute))})))
    aggregates.append(loads(dumps({"operation": "map", "result": dashboard.get_map(create_dict_default_params(institute))})))
    aggregates.append(loads(dumps({"operation": "national_map", "result": dashboard.get_national_map(create_dict_default_params(institute))})))

    collection.insert_many(aggregates)

def add_orientacao_aggregates(db, institute):
    collection = db["orientacao"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "count", "result": orientacao.get_count(create_dict_count_route_params(institute))})))
    aggregates.append(
        loads(dumps({"operation": "keywords", "result": orientacao.get_keywords(create_dict_keywords_route_params(institute))})))
    aggregates.append(loads(dumps({"operation": "map", "result": orientacao.get_map(create_dict_default_params(institute))})))

    types_param = QueryDict(mutable=True)
    default_params = {'institute': institute}
    types_param.update(default_params)
    types_param.setlistdefault('tipos[]', orientacao.get_types())

    aggregates.append(loads(dumps({"operation": "count_by_type", "result": orientacao.get_count(types_param)})))

    collection.insert_many(aggregates)


def add_premios_aggregates(db, institute):
    collection = db["premios_titulos"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "count", "result": premios_titulos.get_count(create_dict_count_route_params(institute))})))

    collection.insert_many(aggregates)


def add_producao_artistica_aggregates(db, institute):
    collection = db["producao_artistica"]
    aggregates = []

    aggregates.append(loads(dumps({ "operation": "count", "result": producao_artistica.get_count(create_dict_count_route_params(institute)) })))
    aggregates.append(loads(dumps({ "operation": "keywords", "result": producao_artistica.get_keywords(create_dict_keywords_route_params(institute)) })))
    aggregates.append(loads(dumps({ "operation": "map", "result": producao_artistica.get_map(create_dict_default_params(institute)) })))

    types_param = QueryDict(mutable=True)
    default_params = {'institute': institute}
    
    types_param.update(default_params)
    aggregates.append(loads(dumps({ "operation": "national_map", "result": producao_artistica.get_national_map(types_param) })))
    

    types_param.setlistdefault('tipos[]', orientacao.get_types())
    aggregates.append(loads(dumps({ "operation": "count_by_type", "result": producao_artistica.get_count(types_param) })))

    collection.insert_many(aggregates)


def add_producao_bibliografica_aggregates(db, institute):
    collection = db["producao_bibliografica"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "count", "result": producao_bibliografica.get_count(create_dict_count_route_params(institute))})))
    aggregates.append(
        loads(dumps({"operation": "keywords", "result": producao_bibliografica.get_keywords(create_dict_keywords_route_params(institute))})))
    aggregates.append(loads(dumps({"operation": "map", "result": producao_bibliografica.get_map(create_dict_default_params(institute))})))
    aggregates.append(
        loads(dumps({"operation": "national_map", "result": producao_bibliografica.get_national_map(create_dict_default_params(institute))})))

    types_param = {'institute': institute,  'tipos': []}
    types_param_dict = QueryDict(mutable=True)
    types_param_dict.update(types_param)
    # types_param.setlistdefault('tipos[]', producao_bibliografica.get_types())
    aggregates.append(loads(dumps({"operation": "count_by_type", "result": producao_bibliografica.get_count(types_param_dict)})))

    collection.insert_many(aggregates)


def add_producao_tecnica_aggregates(db, institute):
    collection = db["producao_tecnica"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "count", "result": producao_tecnica.get_count(create_dict_count_route_params(institute))})))
    aggregates.append(
        loads(dumps({"operation": "keywords", "result": producao_tecnica.get_keywords(create_dict_keywords_route_params(institute))})))
    aggregates.append(loads(dumps({"operation": "map", "result": producao_tecnica.get_map(create_dict_default_params(institute))})))
    aggregates.append(
        loads(dumps({"operation": "national_map", "result": producao_tecnica.get_national_map(create_dict_default_params(institute))})))

    types_param = QueryDict(mutable=True)
    default_params = {'institute': institute}
    types_param.update(default_params)
    types_param.setlistdefault('tipos[]', orientacao.get_types())

    aggregates.append(loads(dumps({"operation": "count_by_type", "result": producao_tecnica.get_count(types_param)})))

    collection.insert_many(aggregates)


def add_pessoa_aggregates(db, institute):
    collection = db["pessoa"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "names", "result": pessoa.get_names()})))

    collection.insert_many(aggregates)

def select_db(institute):
    my_client = pymongo.MongoClient(os.environ.get('AGGREGATE_DB_HOST', 'mongo'), 27017)
    my_client.drop_database(f'{institute}_aggregate')
    db = my_client[f'{institute}_aggregate']
    return db

def create_aggregates(institute):
    print(f'Rodando mongo aggregate para {institute}...')
    db = select_db(institute)
    print(db)
    add_bancas_aggregates(db, institute)
    add_dashboard_aggregates(db, institute)
    add_orientacao_aggregates(db, institute)
    add_premios_aggregates(db, institute)
    add_producao_artistica_aggregates(db, institute)
    add_producao_bibliografica_aggregates(db, institute)
    add_producao_tecnica_aggregates(db, institute)
    add_pessoa_aggregates(db, institute)


# create_aggregates("fearp")
# create_aggregates("fau")
# create_aggregates("ime")
# create_aggregates("fisica")
# create_aggregates("fdrp")
# create_aggregates("iau")
# create_aggregates("io")
# create_aggregates("fzea")
create_aggregates("fo")