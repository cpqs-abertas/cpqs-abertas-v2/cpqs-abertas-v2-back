import psycopg2


# Makes an insert in bancas table
def insert_bancas(attr, conn=None, cur=None):
    """
    Inserts in bancas.

    The insert_bancas function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the bancas table
    :type ev: dict of (<class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id FROM bancas WHERE aluno = %s and ano = %s and titulo = %s", [attr["aluno"], attr["ano"], attr["titulo"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:
            cur.execute('INSERT INTO bancas (aluno, ano, titulo, pais, instituicao, tipo_pesquisa, idioma, doi, curso) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)', [attr["aluno"], attr["ano"], attr["titulo"], attr["pais"], attr["instituicao"], attr["tipo_pesquisa"], attr["idioma"], attr["doi"], attr["curso"]])
            conn.commit()
            cur.execute("SELECT id FROM bancas WHERE aluno = %s and ano = %s and titulo = %s", [attr["aluno"], attr["ano"], attr["titulo"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]
        
        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()


# Makes an insert in rel_pessoa_bancas table
def insert_rel_pessoa_bancas(attr, conn = None, cur = None):
    """
    Inserts in rel_pessoa_bancas.

    The insert_rel_pessoa_bancas function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_bancas table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id FROM rel_pessoa_bancas WHERE pessoa_id = %s and bancas_id = %s", [attr["pessoa_id"], attr["bancas_id"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:
            cur.execute('INSERT INTO rel_pessoa_bancas (pessoa_id, bancas_id) VALUES (%s, %s)', [attr["pessoa_id"], attr["bancas_id"]])
            conn.commit()
            cur.execute("SELECT id FROM rel_pessoa_bancas WHERE pessoa_id = %s and bancas_id = %s", [attr["pessoa_id"], attr["bancas_id"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]

        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()
