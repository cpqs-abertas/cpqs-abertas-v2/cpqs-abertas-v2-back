import psycopg2


# Makes an insert in participacao table
def insert_participacao(attr, conn=None, cur=None):
    """
    Inserts in participacao.

    The insert_participacao function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the participacao table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id FROM participacao WHERE tipo = %s and nome_ev = %s and ano = %s", [attr["tipo"], attr["nome_ev"], attr["ano"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:
            cur.execute('INSERT INTO participacao (nome_ev, tipo, ano, natureza, pais, idioma, meio, flag, doi, nome_inst) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', [attr["nome_ev"], attr["tipo"], attr["ano"], attr["natureza"], attr["pais"], attr["idioma"], attr["meio"], attr["flag"], attr["doi"], attr["nome_inst"]])
            conn.commit()
            cur.execute("SELECT id FROM participacao WHERE tipo = %s and nome_ev = %s and ano = %s", [attr["tipo"], attr["nome_ev"], attr["ano"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]

        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()


# Makes an insert in rel_pessoa_participacao table
def insert_rel_pessoa_participacao(attr, conn=None, cur=None):
    """
    Inserts in rel_pessoa_participacao.

    The insert_rel_pessoa_participacao function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_participacao table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id FROM rel_pessoa_participacao WHERE pessoa_id = %s and participacao_id = %s and titulo = %s", [attr["pessoa_id"], attr["participacao_id"], attr["titulo"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:        
            cur.execute('INSERT INTO rel_pessoa_participacao (pessoa_id, participacao_id, titulo, forma, tipo) VALUES (%s, %s, %s, %s, %s)', [attr["pessoa_id"], attr["participacao_id"], attr["titulo"], attr["forma"], attr["tipo"]])
            conn.commit()
            cur.execute("SELECT id FROM rel_pessoa_participacao WHERE pessoa_id = %s and participacao_id = %s and titulo = %s", [attr["pessoa_id"], attr["participacao_id"], attr["titulo"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]

        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()
