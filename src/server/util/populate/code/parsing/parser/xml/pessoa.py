from html import unescape
from bs4 import BeautifulSoup


def parse_pessoa(root, department, lattes_id):
    pessoa = root.find("DADOS-GERAIS")
    endereco = root.find("DADOS-GERAIS/ENDERECO/ENDERECO-PROFISSIONAL")
    pessoa_dict = {}
    pessoa_dict['id_lattes'] = lattes_id
    pessoa_dict['nome_completo'] = pessoa.attrib['NOME-COMPLETO']
    pessoa_dict['nome_citacao'] = pessoa.attrib['NOME-EM-CITACOES-BIBLIOGRAFICAS']
    pessoa_dict['nacionalidade'] = pessoa.attrib['PAIS-DE-NASCIMENTO']
    pessoa_dict['departamento'] = department

    try:
        pessoa_dict['contato'] = f"+55 ({endereco.attrib['DDD']}) {endereco.attrib['TELEFONE']}"
    except:
        pessoa_dict['contato'] = ""

    try:
        pessoa_dict['cidade'] = pessoa.attrib['CIDADE-NASCIMENTO']
    except:
        pessoa_dict['cidade'] = ""

    try:
        pessoa_dict['estado'] = pessoa.attrib['UF-NASCIMENTO']
    except:
        pessoa_dict['estado'] = ""

    try:
        resumo = pessoa.find("RESUMO-CV")
        pessoa_dict['resumo'] = BeautifulSoup(unescape(resumo.attrib['TEXTO-RESUMO-CV-RH']), 'lxml').text
    except:
        pessoa_dict['resumo'] = ""

    return pessoa_dict
