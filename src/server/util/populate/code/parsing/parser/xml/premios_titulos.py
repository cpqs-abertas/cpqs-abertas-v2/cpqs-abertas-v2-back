from html import unescape
from bs4 import BeautifulSoup


def parse_premios_titulos(root, lattes_id):
    premios_titulos = root.find("DADOS-GERAIS/PREMIOS-TITULOS")
    if premios_titulos is None:
        return []

    pt_array = []
    for pt in premios_titulos:
        formacao_dict = {}
        formacao_dict['id'] = ""
        formacao_dict['pessoa_id'] = lattes_id
        formacao_dict['nome'] = BeautifulSoup(unescape(pt.attrib['NOME-DO-PREMIO-OU-TITULO']), 'lxml').text
        formacao_dict['ano'] = pt.attrib['ANO-DA-PREMIACAO']
        formacao_dict['entidade'] = pt.attrib['NOME-DA-ENTIDADE-PROMOTORA']

        pt_array.append(formacao_dict)

    return pt_array
