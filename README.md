# CPQs Abertas

## Sobre o projeto

O projeto FAU Aberta (nome provisório) foi criado no segundo semestre de 2019 
para disciplina de Laboratório de Métodos Ágeis, ministrada no Instituto de 
Matemática e Estatística da Universidade de São Paulo.

Em 2021, o projeto evoluiu para CPQs Abertas, buscando abranger todas as unidades da USP.

O intuito do projeto é aumentar a visibilidade das produções realizadas por cada unidade da USP, atráves da analise dos dados retirados da [plataforma Lattes](http://lattes.cnpq.br/).

Para saber mais sobre o projeto, veja a nossa [wiki](https://gitlab.com/availablenick/cpqs-abertas/-/wikis/home).
